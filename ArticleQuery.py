from flask import Flask, request
from bs4 import BeautifulSoup
import urllib2
from flask import jsonify
from flask_cors import CORS
import re

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


if __name__ == '__main__':
    app.run(debug=True, port=5000)

@app.route('/query-article', methods=['POST'])
def queryArticle():
    req_data = request.get_json()
    keyword = req_data['keyword']

    # keyword string contains - like  1.) what is IKET 2.) IKET

    # NER or POS tagging

    prefixes = ('From', 'To', '_NextPart', '-', '[', ']', '{', '}', '/', '\'')

    rawOutputFromBSoup = parseHTML(keyword=keyword);

    filteredOutputFromBSoup = []

    for outputLine in rawOutputFromBSoup:
        symbolsfound = False
        for characterCheck in prefixes:
            if characterCheck in outputLine:
                symbolsfound = True
                continue
        if(symbolsfound == False):
            filteredOutputFromBSoup.append(outputLine)

    finalOutputString = []

    for line in filteredOutputFromBSoup:

        if line.startswith(prefixes):
            output.remove(line)
            continue

        lineString = '';

        splitwords = line.split()

        for word in splitwords:
            if word.endswith('20'):
                word = word[:-2]
            lineString = lineString + ' ' + word

        finalOutputString.append(lineString)

    finalOutputString = '\n'.join(finalOutputString)

    return jsonify(type = 'text',
                    message = finalOutputString,
                    reply = False,
                    sender = 'Bot',
                    avatar = '');

def parseHTML(keyword):
    with open("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/chatBot/webScraping/AdMasterInternalKnowledgeEducationAndTraining_IKET.mht") as mht_file:
        data = mht_file.read()
        data_n = data.replace('\n', '')
        data_n = data.replace('=', '')
        soup = BeautifulSoup(data_n, 'html.parser')

        output = soup(text=re.compile(keyword))
    return output;

# FLASK_APP=ArticleQuery.py flask run
# http://127.0.0.1:5000/query-article   POST call
# {
# 	"keyword": "what is AdMaster"
# }
# to Do
# 1. Keyword( query String) extraction from user input - verbs
# 2. extrating keyword match strings from multiple pages
# 3. code clean up with pattern matching
